angular.module('starter.controllers', [])

.controller('MapCtrl', function($scope, $ionicLoading) {
  $scope.mapCreated = function(map) {
    $scope.map = map;
    $scope.centerOnMe();
    $scope.getCurrentPosition();
  };

  $scope.centerOnMe = function () {
    console.log("Centering");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
  };

  $scope.getCurrentPosition = function() {
    var flightPlanCoordinates = [];

    setInterval(function() {
      navigator.geolocation.getCurrentPosition(function (pos) {
        flightPlanCoordinates.push({lat: pos.coords.latitude, lng: pos.coords.longitude});
        
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          geodesic: true,
          strokeColor: '#FFFF7F',
          strokeOpacity: 1.0,
          strokeWeight: 13
        });

        flightPath.setMap($scope.map);
      }, function (error) {
        alert('Unable to get location: ' + error.message);
      });
    }, 10000);
  };
});
